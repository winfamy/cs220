/** bigO.cpp
* ===========================================================
* Name: Grady Phillips, 21 Jan, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Type {
	int num;
	char key[20];
} Type;

void rand_str(char *dest, size_t length) {
	char charset[] = "0123456789"
	                 "abcdefghijklmnopqrstuvwxyz"
	                 "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	while (length-- > 0) {
		size_t index = (double) rand() / RAND_MAX * (sizeof charset - 1);
		*dest++ = charset[index];
	}
	*dest = '\0';
}

void InsertionSort(Type arr[], int c) {
	int i, j;
	Type key;
	for (i = 1; i < c; i++)
	{
		key = arr[i];
		j = i - 1;

		while (j >= 0 && arr[j].num > key.num)
		{
			arr[j + 1] = arr[j];
			j = j - 1;
		}
		arr[j + 1] = key;
	}
}

int main() {
	int c = 20;
	Type * arr = malloc(c * sizeof(Type));

	char a[20];
	printf("Unsorted\n[");
	for (int i = 0; i < c; ++i) {
		arr[i].num = rand() % 10 + 1;
		rand_str(a, 2);
		strcpy(arr[i].key, a);

		printf("%s %d, ", arr[i].key, arr[i].num);
	}
	printf("]\n");



	InsertionSort(arr, c);
	printf("Sorted\n[");
	for (int i = 0; i < c; ++i) {
		printf("%s %d, ", arr[i].key, arr[i].num);
	}
	printf("]\n");

	return 0;
}