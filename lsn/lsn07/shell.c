/** shell.c
* ===========================================================
* Name: Grady Phillips, 27 Jan, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#include <stdlib.h>
#include <stdio.h>
#define ARR_SIZE 22

void printArr(int a[]) {
	for (int i = 0; i < ARR_SIZE; ++i) {
		printf("%d ", a[i]);
	}

	printf("\n");
}

void shellSort(int array[]) {
	int temp;
	for (int gap = ARR_SIZE / 2; gap > 0; gap /= 2) {
		for (int i = gap; i < ARR_SIZE; i += 1) {
			temp = array[i];
			int j = i;
			while (j >= gap && array[j - gap] > temp) {
				array[j] = array[j - gap];
				j -= gap;
			}

			array[j] = temp;
			printArr(array);
		}
	}

	// return arr;
}


int main() {
	int * a;
	a = malloc(ARR_SIZE * sizeof(int));

	for (int i = 0; i < ARR_SIZE; ++i) {
		a[i] = rand() % 10 + 1;
	}

	printArr(a);
	shellSort(a);
	printArr(a);


    return 0;
}