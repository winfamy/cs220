/** lab11.c
* ================================================================
* Name: First, Last
* Section:
* Project: Lab11 - Queue ADT implemented with C array
* =================================================================
*
* Instructions:  This lab is similar to the stack ADT implementation
* only with a circular queue.  You will insert appropriate function
* calls in the template below to implement a waiting ticket system ala your
* local DMV.
*
* Complete the tasks below after completing the tasks in the queueAsArray.c
* file
*/
#include "queue.h"
#include "totallyneededlibrary.h"
#include <stdio.h>
#include <stdlib.h>
#include <zconf.h>

//local function prototypes
void printMenu(Queue * q);
int main() {

	/** TASK 0 - Add functionality to this file to enable the DMV
	 * application to run.
	 * 0) ensure that you have completed implementation of
	 * the functions in queueAsArray.c prior to starting this task.
	 * 1) Call appropriate functions in the switch statement below
	 * 2) use printQueue to test your functionality
	 * Note: you MUST use the queue library in completing
	 * this task.
	 */

	Queue q;
	queueInit(&q);

	// handleEdgeCase();
	int choice = 0;
	do
	{
		printMenu(&q);
		scanf("%d",&choice);
		switch (choice) {
			case 1: { // take a number - generate a random number between 1 and 50 then enqueue
				int r = rand() % 50 + 1;
				enqueue(&q, r);
				break;
			}
			case 2: { // dequeue the number at the front of the line and print it
				int x;
				dequeue(&q, &x)
				? printf("%d\n", x)
				: printf("%s\n", "Dequeue failed.\n");
				break;
			}
			case 3: { // display the total number of people waiting
				printf("%d\n", q.c);
				break;
			}
			case 4: { // display the current wait list from fist to last
				printQueue(&q);
				break;
			}
			case 5: { // exit the application
				break;
			}
			default: { // invalid choice
			}
		}
	} while (choice != 5);

	return 0;
}

/** printMenu() - prints a menu for our DMV app
 */
void printMenu(Queue * q) {
	printf("\n\n      Menu\n");
	printf("Take a number-----1\n");
	printf("Call a number-----2\n");
	printf("Number waiting----3\n");
	printf("Display waiting---4\n");
	printf("Exit--------------5\n\n");
	printQueue(q);
	printf("Enter option: ");
}

bool isFull(Queue * q) {
	return q->c == QUEUE_SIZE;
}

void printQueue(Queue * q) {
	printf("[ ");
	if (q->c == 0)
		printf("]\n");

	for (int i = q->h; i <= q->t; ++i) {
		printf(i == q->t ? "%d ]\n\n" : "%d, ", q->items[i]);
	}
}

void queueInit(Queue * q) {
	q->t = -1;
	q->h = 0;
	q->c = 0;
	q->s = QUEUE_SIZE;
}

int enqueue(Queue * q, int x) {
	if (q->c == q->s) {
		handleEdgeCase(q);
		printf("Handled edge case successfully\n");
		return 1;
	}

	q->t++;
	q->items[q->t] = x;
	q->c++;

	return 1;
}

int dequeue(Queue * q, int * x) {
	if (q->c == 0) {
		handleEdgeCase(q);
		printf("Handled edge case successfully\n");
		return 1;
	}

	*x = q->items[q->h];
	for (int i = q->h; i < q->t; ++i) {
		q->items[i] = q->items[i + 1];
	}

	q->t--;
	q->c--;

	return 0;
}