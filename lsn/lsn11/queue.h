/** queue.h
* ===========================================================
* Name: Grady Phillips, 06 Feb, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#ifndef CS220_QUEUE_H
#define CS220_QUEUE_H

#include <stdbool.h>
#define QUEUE_SIZE 10

typedef struct Queue {
	int h;
	int t;
	int s;
	int c;
	int items[QUEUE_SIZE];
} Queue;

/*
 * Function: enqueue
 * ----------------------------
 *   Adds an item to the queue,
 *   if it encounters an edge case
 *   the queue will self-destruct
 *
 *   q: queue pointer
 *   x: the value to add
 *
 *   returns: 0 on error, 1 on success
 */
int enqueue(Queue * q, int x);

/*
 * Function: dequeue
 * ----------------------------
 *   Removes first item from the queue,
 *   if it encounters an edge case
 *   the queue will self-destruct
 *
 *   q: queue pointer
 *   x: the value dequeued
 *
 *   returns: 0 on error, 1 on success
 */
int dequeue(Queue * q, int * x);
void printQueue(Queue * q);
bool isFull(Queue * q);
void queueInit(Queue * q);

#endif //CS220_QUEUE_H
