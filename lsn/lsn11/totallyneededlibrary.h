/** explosion.h
* ===========================================================
* Name: Grady Phillips, 06 Feb, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#ifndef CS220_TOTALLYNEEDEDLIBRARY_H
#define CS220_TOTALLYNEEDEDLIBRARY_H

#define NUM_FRAMES 150
#define NUM_BLOBS 800
#define PERSPECTIVE 50.0
#include "queue.h"

typedef struct {
	double x,y,z;
} spaceblob;

int handleEdgeCase(Queue * q);
double prng();
int explode();

#endif //CS220_TOTALLYNEEDEDLIBRARY_H
