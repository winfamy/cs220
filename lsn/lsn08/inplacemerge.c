/** inplacemerge.c
* ===========================================================
* Name: Grady Phillips, 29 Jan, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#include <stdlib.h>
#include <stdio.h>

void printArr(int a[], int n) {
	for (int i = 0; i < n; ++i) {
		printf("%d ", a[i]);
	}

	printf("\n");
}

void merge(int *, int s, int m, int e);
void mergeSort(int *, int l, int r);

int main() {

	int arr[] = {1,2,3,4,5,6,7,1,13,14,1,4,2,-41,12,4176,123456,123,612};
	mergeSort(arr, 0, sizeof(arr) / sizeof(arr[0]) - 1);
	printArr(arr, sizeof(arr) / sizeof(arr[0]) - 1);

    return 0;
}

void mergeSort(int * arr, int l, int r) {
	if (l < r) {
		int m = (r - l) / 2;

		printArr(arr, r - l + 1);
		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);

		merge(arr, l, m, r);
	}
}

void merge(int * arr, int s, int m, int e) {
	while (s <= m && m + 1 <= e) {
		// take left
		if (arr[s] <= arr[m + 1]) {
			s++;
			continue;
		}

		// take right
		int savedVal = arr[m + 1];
		int shiftI = m + 1;
		printf("%d %d %d\n", s, m, e);
		while (--shiftI != s)
			arr[shiftI] = arr[shiftI - 1];

		arr[s] = savedVal;
		s++; m++;
	}
}