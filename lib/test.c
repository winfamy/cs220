/** test.c
* ===========================================================
* Name: Grady Phillips, 14 Feb, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#include "ll.h"
#include "stack.h"
#include <stdio.h>

int main() {

	double x = 0;
	Stack * s = createStack();
	push(s, 1);
	printStack(s);
	push(s, 2);
	printStack(s);
	pop(s, &x);
	printf("%lf\n", x);
	printStack(s);
	pop(s, &x);
	printf("%lf\n", x);
	printStack(s);

    return 0;

}