/** ll.c
* ===========================================================
* Name: Grady Phillips, 12 Feb, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	Define the functions relevant
* 	to linked list library.
* ===========================================================
*/

#include "ll.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

LL * createList(int elemSize) {
	LL * l = malloc(sizeof(LL));

	l->head = NULL;
	l->size = 0;
	l->elemSize = elemSize;

	return l;
}

N * createNode(void * x, int elemSize) {
	N * n = malloc(sizeof(N));
	n->next = NULL;
	n->last = NULL;
	n->i = malloc(elemSize);
	memcpy(n->i, x, elemSize);

	return n;
}

int append(LL * l, void * x) {
	N * n = l->head;
	N * newNode = createNode(x, l->elemSize);

	if (!isEmpty(l)) {
		while (n->next != NULL)
			n = n->next;

		n->next = newNode;
		newNode->last = n;
	} else {
		l->head = newNode;
	}

	l->size++;
	return LL_SUCCESS;
}

int insertAfter(LL * l, int i, void * x) {
	if (i >= l->size) return LL_FAILURE;

	N * n = l->head;
	for (int j = 0; j < i; ++j) {
		n = n->next;
	}

	N * new = createNode(x, l->elemSize);
	new->next = n->next;
	new->last = n;
	n->next = new;
	l->size++;

	return LL_SUCCESS;
}

int prepend(LL * l, void * x) {
	N * n = l->head;
	N * new = createNode(x, l->elemSize);

	if (!isEmpty(l)) {
		l->head = new;
		new->next = n;
		n->last = new;
	} else {
		l->head = new;
	}

	l->size++;
	return LL_SUCCESS;
}

// 1 2 3 4 5
// 0 1 2 3 4
int delete(LL * l, int i, void * x) {
	if (i >= l->size) return LL_FAILURE;

	N * n = l->head;
	for (int j = 0; j < i; ++j) {
		n = n->next;
	}

	if (n == l->head) {
		l->head = n->next;
	} else {
		n->last->next = n->next;
	}

	if (n->next != NULL)
		n->next->last = NULL;

	memcpy(x, n->i, l->elemSize);
	l->size--;
	free(n);
	return LL_SUCCESS;
}

int find(LL * l, void * x) {
	N * n = l->head;
	for (int i = 0; i < l->size; ++i) {
		if (n->i == x)
			return i;

		n = n->next;
	}

	return -1;
}

void * elementAt(LL * l, int i) {
	if (i >= l->size) return NULL;
	N * n = l->head;

	for (int j = 0; j < i; ++j) {
		n = n->next;
	}

	return n ? n->i : NULL;
}

int isEmpty(LL * l) {
	return l->size == 0;
}

int deleteList(LL * l) {
	N * n = l->head;
	while (n && n->next != NULL) {
		n = n->next;
	}

	while (n && n->last != NULL) {
		n = n->last;
		free(n->next);
	}

	free(l);
	return LL_SUCCESS;
}

void printList(LL * l) {
	N * n = l->head;
	for (int i = 0; i < l->size; ++i) {
		printf(i != l->size - 1 ? "%c -> " : "%c", *(char *)n->i);
		n = n->next;
	}
	printf("\n");
}