/** ll.h
* ===========================================================
* Name: Grady Phillips, 12 Feb, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	Declare the functions related to a stack ADT.
* ===========================================================
*/

#ifndef CS220_STACK_H
#define CS220_STACK_H

#define STACK_SUCCESS 1
#define STACK_FAILURE 0

#include "ll.h"

typedef struct Stack {
    LL * list;
} Stack;

/**   ----------------------------------------------------------
*   createStack
*   Initializes a stack
*
*   returns: pointer to stack
*/
Stack * createStack(int elemSize);

/**   ----------------------------------------------------------  
*   push
*   Pushes a value onto the top of the stack
*     
*   s: stack
*   x: the value to be pushed
*   
*   returns: STACK_SUCCESS or STACK_FAILURE
*/ 
int push(Stack * s, void * x);

/**   ----------------------------------------------------------
*   pop
*   Pops a value from the top of the stack into * x
*
*   s: the stack
*   x: the value to store in
*
*   returns: STACK_SUCCESS or STACK_FAILURE
*/
int pop(Stack * s, void * x);

/**   ----------------------------------------------------------  
*   height
*   Returns the height of the stack
*     
*   s: stack
*   
*   returns: height
*/ 
int height(Stack * s);

/**   ----------------------------------------------------------  
*   printStack
*   prints a stack
*     
*  	s: stack
*   
*   returns: void
*/ 
void printStack(Stack * s);

/**   ----------------------------------------------------------  
*   getTop
*   Gets the top of the stack
*     
*   s: stack  
*   
*   returns: top node
*/ 
void * getTop(Stack * s);

/**   ----------------------------------------------------------  
*   deleteStack
*   Deletes a stack
*     
*   s: stacl  
*   
*   returns: void
*/
void deleteStack(Stack * s);

#endif //CS220_LL_H