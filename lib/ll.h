/** ll.h
* ===========================================================
* Name: Grady Phillips, 12 Feb, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	Declare the types and functions relevant
* 	to linked list library.
* ===========================================================
*/

#ifndef CS220_LL_H
#define CS220_LL_H

#define LL_SUCCESS 22
#define LL_FAILURE 23

typedef struct N {
	struct N * next;
	struct N * last;
	void * i;
} N;

typedef struct LL {
	struct N * head;
	int size;
	int elemSize;
} LL;


/**   ----------------------------------------------------------
*   createList
*   Initializes a list
*
*   returns: list
*/
LL * createList(int elemSize);

/**   ----------------------------------------------------------  
*   createNode
*   Initializes a node to be used in the list
*     
*   val: the value for the node
*   
*   returns: node
*/ 
N * createNode(void * val, int elemSize);

/**   ----------------------------------------------------------  
*   append
*   Appends a value at the end of the list
*
*   l: the list to append to
*   x: the value to append
*   
*   returns: LL_SUCCESS or LL_FAILURE
*/ 
int append(LL * l, void * x);

/**   ----------------------------------------------------------
*   insert
*   Inserts a value into list l at position i with value x
*
*	l: the list
*	i: position (0-indexed)
*	x: the value
*
*   returns: LL_SUCCESS or LL_FAILURE
*/
int insertAfter(LL * l, int i, void * x);

/**   ----------------------------------------------------------  
*   prepend
*   Prepends value x to list l
*     
*   l: list
*   x: the value
*   
*   returns: LL_SUCCESS or LL_FAILUREs
*/ 
int prepend(LL * l, void * x);

/**   ----------------------------------------------------------
*   delete
*   Deletes node at value i and sets x to that value
*
*   l: list
*   i: the position
*   x: the pointer to the value deleted
*
*   returns: LL_SUCCESS or LL_FAILURE
*/
int delete(LL * l, int i, void * x);

/**   ----------------------------------------------------------
*   find
*   Finds the first instance of x
*
*   l: the list
*   x: the value to be found
*
*   returns: the index of the first instance of x
*/
int find(LL * l, void * x);

/**   ----------------------------------------------------------
*   elementAt
*   Returns the element at position i of the list
*
*   l: the list
*   i: the position
*
*   returns: the value at that position
*/
void * elementAt(LL * l, int i);

/**   ----------------------------------------------------------  
*   isEmpty
*   Returns true if list is empty
*     
*   l: list to check  
*   
*   returns: isEmpty lol  
*/ 
int isEmpty(LL * l);

/**   ----------------------------------------------------------  
*   deleteList
*   frees all nodes in list then list itself
*     
*   l: list
*   
*   returns: LL_SUCCESS or LL_FAILURE
*/ 
int deleteList(LL * l);

/**   ----------------------------------------------------------  
*   printList
*   Prints the list...
*     
*   l: list to print
*   
*   returns: void
*/ 
void printList(LL * l);

#endif //CS220_LL_H
