/** pex3.c
* ===========================================================
* Name: Grady Phillips, 2 Mar, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	This is the driver for PEX3
* Documentation:
* 	None
* ===========================================================
*/

#include "calcDisplay.h"

int main () {
	calculator_driver();
    return 0;
}