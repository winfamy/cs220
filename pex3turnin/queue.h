/** queue.h
* ===========================================================
* Name: Grady Phillips, 19 Mar, 2020
* Section: T6A
* Project: PEX 3
* Purpose:
* 	Implement a queue library built on a linked list.
* ===========================================================
*/

#ifndef CS220_QUEUE_H
#define CS220_QUEUE_H

#define QUEUE_SUCCESS 1
#define QUEUE_FAILURE 0

#include "ll.h"

typedef struct Queue {
	LL * list;
} Queue;

/**   ----------------------------------------------------------
*   createQueue
*   Initializes a queue
*
*   returns: pointer to queue
*/
Queue * createQueue(int elemSize);

/**   ----------------------------------------------------------
*   queue
*   Adds a node to the end of the queue
*
*   q: queue
*   x: the value to be queue
*
*   returns: QUEUE_SUCCESS or QUEUE_FAILURE
*/
int queue(Queue * q, void * x);

/**   ----------------------------------------------------------
*   dequeue
*   Dequeue the first element and place it in *x
*
*   q: queue
*   x: the value to store in
*
*   returns: QUEUE_SUCCESS or QUEUE_FAILURE
*/
int dequeue(Queue * q, void * x);

/**   ----------------------------------------------------------
*   queueSize
*   Returns the size of the queue
*
*   q: queue
*
*   returns: size
*/
int queueSize(Queue * q);

/**   ----------------------------------------------------------
*   printQueue
*   prints a queue
*
*  	q: queue
*
*   returns: void
*/
void printQueue(Queue * q);

/**   ----------------------------------------------------------
*   getFirst
*   Gets the front of the queue
*
*   q: queue
*
*   returns: node at front of queue
*/
void * getFirst(Queue * q);

/**   ----------------------------------------------------------
*   deleteQueue
*   frees a queue and its items
*
*   q: queue
*
*   returns:
*/
void deleteQueue(Queue * q);

#endif //CS220_QUEUE_H
