/** stack.c
* ===========================================================
* Name: Grady Phillips, 12 Feb, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	Declare the functions related to a stack ADT.
* ===========================================================
*/

#include <stdlib.h>
#include "ll.h"
#include "stack.h"

Stack * createStack(int elemSize) {
    Stack * s = malloc(sizeof(Stack));
    s->list = createList(elemSize);
    return s;
}

int push(Stack * s, void * x) {
    return append(s->list, x);
}

int pop(Stack * s, void * x) {
    if (isEmpty(s->list))
        return 1;
    
    return delete(s->list, s->list->size - 1, x);
}

void * getTop(Stack * s) {
	return elementAt(s->list, s->list->size - 1);
}

int height(Stack * s) {
    return s->list->size;
}

void printStack(Stack * s) {
    printList(s->list);
}

void deleteStack(Stack * s) {
    deleteList(s->list);
    free(s);
}