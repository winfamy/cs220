/** calcDisplay.h
* ===========================================================
* Name: Grady Phillips, 15 Mar, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#ifndef CS220_CALCDISPLAY_H
#define CS220_CALCDISPLAY_H

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#include <stdlib.h>
#include <ncurses.h>
#include <form.h>
#include <menu.h>

void calculator_driver();

#endif //CS220_CALCDISPLAY_H
