/** rpn.c
* ===========================================================
* Name: Grady Phillips, 12 Feb, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	This is the implementation file for the functions
* 	used to parse and interpret RPN.
* ===========================================================
*/

#include "rpn.h"
#include "../../lib/stack.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

double ctod(char c) {
    return (double)(c - '0');
}

int stoi(char * str) {
	int res = 0;
	while (strlen(str) != 0) {
		res += (int)(ctod(str[0])) * pow(10, strlen(str) - 1);
		str++;
	}

	return res;
}

double evaluate(Stack * s, char * str) {
    double result;
    if (s == NULL) {
    	s = createStack(sizeof(double));
    }

    int lengthUsed = 1;
    if (str[0] == '\0') {
        pop(s, &result);
        return result;
    }

    if (strchr("+/*^-", str[0])) {
        if (str[0] == '-') {
            if (isdigit(str[1])) {
                result = - parseDigits(str + 1, &lengthUsed);
                push(s, &result);

                return evaluate(s, str + lengthUsed + 1);
            }
        }

        double x, y;

        // top goes into y, stack reverses order
        pop(s, &y);
        pop(s, &x);
        result = handleOperator(str[0], x, y);
        push(s, &result);
    }

    if (isdigit(str[0])) {
    	result = parseDigits(str, &lengthUsed);
    	push(s, &result);
    }

    return evaluate(s, str + lengthUsed);
}


double handleOperator(char op, double arg1, double arg2) {
    // printf("%lf %c %lf\n", arg1, op, arg2);
    switch(op) {
        case '+':
            return arg1 + arg2;
        case '-':
            return arg1 - arg2;
        case '*':
            return arg1 * arg2;
        case '/':
            return arg1 / arg2;
        case '^':
            return pow(arg1, arg2);
    	default:
    		return 1;
    }
}

double parseDigits(char * str, int * length) {
	int numChecked = 0,
		denLength = 0;
	double num, den = 0;

	num = ctod(str[0]);
	while (isdigit(str[1 + numChecked]) || str[1 + numChecked] == '/') {
		if (str[1 + numChecked] == '/') {
			den = parseDigits(str + 2 + numChecked, &denLength);

			// first char and / aren't counted in numChecked/denLength
			*length = numChecked + denLength + 2;
			return num / den;
		}

		num *= 10;
		num += ctod(str[1 + numChecked]);

		numChecked++;
	}

	*length = numChecked + 1;
	return num;
}