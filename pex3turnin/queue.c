/** queue.c
* ===========================================================
* Name: Grady Phillips, 19 Mar, 2020
* Section: T6A
* Project: PEX 3
* Purpose:
* 	Implement a queue library built on a linked list.
* ===========================================================
*/

#include <stdlib.h>
#include "queue.h"

Queue * createQueue(int elemSize) {
	Queue * q = malloc(sizeof(Queue));
	q->list = createList(elemSize);

	return q;
}

int queue(Queue * q, void * x) {
	return append(q->list, x);
}

int dequeue(Queue * q, void * x) {
	if (isEmpty(q->list)) {
		return QUEUE_FAILURE;
	}

	return delete(q->list, 0, x);
}

int queueSize(Queue * q) {
	return q->list->size;
}

void printQueue(Queue * q) {
	printList(q->list);
}

void * getFirst(Queue * q) {
	return elementAt(q->list, 0);
}

void deleteQueue(Queue * q) {
	deleteList(q->list);
	free(q);
}