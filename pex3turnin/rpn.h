/** rpn.h
* ===========================================================
* Name: Grady Phillips, 12 Feb, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	This is the header file for the functions used to
* 	parse and interpret RPN.
* ===========================================================
*/

#ifndef rpn_h
#define rpn_h

#include "stack.h"

/**   ----------------------------------------------------------
*   evaluate
*   Interprets a string in valid RPN, can take any operators or
*   positive and negative rational numbers.
*
*   s: the stack implementation
*   str: the string to be evaluated
*
*   returns: value of RPN string as double
*/
double evaluate(Stack * s, char * str);

/**   ----------------------------------------------------------
*   handleOperator
*   Uses an input math operator as a character and applies
*   that operations on the two given parameters
*
*   op: the operation to perform
*   arg1: first arg
*   arg2: second arg
*
*   returns: the value of the operation
*/
double handleOperator(char op, double arg1, double arg2);

/**   ----------------------------------------------------------
*   parseDigits
*   Handles the extraction of numeric values from a string
*   to include rational numbers.
*
*   str: the string to extract the value from
*   length: the number of chars taken up by the numeric value
*
*   returns: double value of the number
*/
double parseDigits(char * str, int * length);

/**   ----------------------------------------------------------
*   ctod
*   Gets the value of a digit char as a double
*
*   c: the character
*
*   returns: the double value of the char c
*/
double ctod(char c);

/**   ----------------------------------------------------------
*   stoi
*   Gets the value of a digit char as an int
*
*   c: the character
*
*   returns: the int value of the char c
*/
int stoi(char * s);


#endif