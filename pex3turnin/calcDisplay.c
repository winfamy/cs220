/** calcDisplay.c
* ===========================================================
* Name: Grady Phillips, 15 Mar, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#include "calcDisplay.h"
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#include <stdlib.h>
#include <ncurses.h>
#include <form.h>
#include <menu.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <zconf.h>
#include "shuntingAlgo.h"
#include "rpn.h"

void calculator_driver() {
	int winWidth = 40,
		winHeight = 20;
	int scrRows, scrCols;
	int winStartX, winStartY;

	int c;
	bool isDone = false;
	char input[1024];
	char rpn[1024];
	FORM * eqForm;
	MENU * calcMenu;
	ITEM ** calc_items;
	WINDOW *calcMenuWin, *calcEqnWin, *calcResultWin, *calcRPNWin;
	int rows, cols, n_items;
	char * menuOptions[] = {
			"1", "2", "3", "+",
			"4", "5", "6", "-",
			"7", "8", "9", "*",
			"_", "(", ")", "/",
			"[X]", "[Clr]", "[Run]", ""
	};

	initscr();
	keypad(stdscr, true);
	noecho();
	start_color();
	cbreak();
	curs_set(0);

	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
	// init_pair(3, COLOR_BLACK, COLOR_GREEN);

	n_items = ARRAY_SIZE(menuOptions);
	calc_items = calloc(n_items, sizeof(ITEM *));
	for (int i = 0; i < n_items; ++i) {
		if (menuOptions[i][0] == '_') {
			calc_items[i] = new_item("_", " ");
		} else {
			calc_items[i] = new_item(menuOptions[i], menuOptions[i]);
		}
	}


	// setup win layouts
	getmaxyx(stdscr, scrRows, scrCols);
	winStartX = (scrCols - winWidth) / 2;
	winStartY = (scrRows - winHeight) / 2;
	calcMenuWin = newwin(winHeight, winWidth, winStartY, winStartX);
	box(calcMenuWin, 0, 0);

	// derwin = newwin but defined with x/y relative to parent
	calcEqnWin = derwin(calcMenuWin, 3, winWidth - 2, 3, 1);
	box(calcEqnWin, 0, 0);
	calcRPNWin = derwin(calcMenuWin, 3, winWidth - 2, 6, 1);
	box(calcRPNWin, 0, 0);
	calcResultWin = derwin(calcMenuWin, 3, winWidth - 2, 9, 1);
	box(calcResultWin, 0, 0);

	// setup menu & menu options
	calcMenu = new_menu(calc_items);
	keypad(calcMenuWin, true);
	menu_opts_off(calcMenu, O_SHOWDESC);
	set_menu_win(calcMenu, calcMenuWin);
	set_menu_sub(calcMenu, derwin(calcMenuWin, 5, winWidth - 2, 13, 2));
	set_menu_mark(calcMenu, "> ");
	set_menu_format(calcMenu, 5, 4);
	set_menu_fore(calcMenu, COLOR_PAIR(2));

	// setup the top menu label thing
	mvwprintw(calcMenuWin, 1, 3, "%s", "Super Duper Calculator");
	mvwaddch(calcMenuWin, 2, 0, ACS_LTEE);
	mvwhline(calcMenuWin, 2, 1, ACS_HLINE, 39);
	mvwaddch(calcMenuWin, 2, 39, ACS_RTEE);

	// setup the field labels
	mvwprintw(calcMenuWin, 3, 3, "%s", " Equation Input ");
	mvwprintw(calcMenuWin, 6, 3, "%s", " RPN Result ");
	mvwprintw(calcMenuWin, 9, 3, "%s", " Double Result ");

	refresh();

	post_menu(calcMenu);
	wrefresh(calcMenuWin);
	wrefresh(calcEqnWin);
	wrefresh(calcRPNWin);

	while((c = wgetch(calcMenuWin)) != KEY_F(1) && !isDone)
	{
		mvwhline(calcMenuWin, 4, 2, ' ', 34);
		switch(c) {
			case KEY_DOWN:
				menu_driver(calcMenu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(calcMenu, REQ_UP_ITEM);
				break;
			case KEY_RIGHT:
				menu_driver(calcMenu, REQ_RIGHT_ITEM);
				break;
			case KEY_LEFT:
				menu_driver(calcMenu, REQ_LEFT_ITEM);
				break;

			case 48:
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
			case 54:
			case 55:
			case 56:
			case 57:
				sprintf(rpn, "%d", c - '0');
				strcat(input, rpn);
				break;

			case KEY_BACKSPACE:
			case 127:
			case '\b':
				input[strlen(input) - 1] = '\0';
				break;

			case '+':
			case '-':
			case '/':
			case '*':
			case ' ':
			case '(':
			case ')':
				sprintf(rpn, "%c", c);
				strcat(input, rpn);
				break;

			case 10:
				if (current_item(calcMenu)->description.str[0] == '[') {
					if (strstr(current_item(calcMenu)->description.str, "Clr")) {
						strcpy(input, "");
					} else if (strstr(current_item(calcMenu)->description.str, "Run")) {
						strcpy(rpn, "");
						strcpy(rpn, convert(input));
						mvwhline(calcRPNWin, 1, 2, ' ', winWidth - 5);
						mvwhline(calcResultWin, 1, 2, ' ', winWidth - 5);
						mvwprintw(calcRPNWin, 1, 2, "%s", rpn);
						mvwprintw(calcResultWin, 1, 2, "%lf", evaluate(NULL, rpn));
						wrefresh(calcRPNWin);
						wrefresh(calcResultWin);
					} else {
						isDone = true;
					}
				} else {
					strcat(input, current_item(calcMenu)->description.str);
				}
				break;
			default:
				mvwprintw(calcMenuWin, 4, 2, "%s", " I don\'t know that key.");
				wrefresh(calcMenuWin);
				sleep(1);
		}

		mvwhline(calcMenuWin, 4, 2, ' ', winWidth - 5);
		mvwprintw(calcMenuWin, 4, 2, " %s", input);
		wrefresh(calcMenuWin);
	}

	unpost_menu(calcMenu);
	free_menu(calcMenu);
	for (int j = 0; j < n_items; ++j) {
		free_item(calc_items[j]);
	}

	delwin(calcMenuWin);
	delwin(calcEqnWin);
	delwin(calcRPNWin);
	delwin(calcResultWin);
	endwin();
}