/** shuntingAlgo.c
* ===========================================================
* Name: Grady Phillips, 02 Mar, 2020
* Section: T6A
* Project: 
* Purpose: 
* ===========================================================
*/

#include "../../lib/stack.h"
#include "shuntingAlgo.h"
#include "rpn.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

char * convert (char * str) {
	Stack * opStack = createStack(sizeof(char));
	char * output = malloc(64 * sizeof(str));

	return convertToDumbRPN(str, output, opStack);
}

char * convertToDumbRPN(char * str, char * output, Stack * opStack) {
	char token[22];
	char tempOut[128];
	char op, poppedOp;

// 		if the token is a function then:
// 		    push it onto the operator stack
// 		if the token is an operator, then:
// 			while ((there is a function at the top of the operator stack)
// 			    or (there is an operator at the top of the operator stack with greater precedence)
// 			    or (the operator at the top of the operator stack has equal precedence and the token is left associative))
// 			    and (the operator at the top of the operator stack is not a left parenthesis):
// 		        pop operators from the operator stack onto the output queue.
// 			push it onto the operator stack.
// 		if the token is a left paren (i.e. "("), then:
// 		    push it onto the operator stack.
// 		if the token is a right paren (i.e. ")"), then:
// 		    while the operator at the top of the operator stack is not a left paren:
// 		        pop the operator from the operator stack onto the output queue.
// 		    /* If the stack runs out without finding a left paren, then there are mismatched parentheses. */
// 		    if there is a left paren at the top of the operator stack, then:
// 		        pop the operator from the operator stack and discard it
// 	/* After while loop, if operator stack not null, pop everything to output queue */
//  if there are no more tokens to read then:
// 		while there are still operator tokens on the stack:
// 		/* If the operator token on the top of the stack is a paren, then there are mismatched parentheses. */
// 		    pop the operator from the operator stack onto the output queue.
//  exit.

	if (!isValidPostfix(str, "")) {
		strcpy(output, "err: input not valid");
		return output;
	}

	//  while there are tokens to be read do:
	while (str[0] != '\0') {
		// read a token.
		strcpy(token, getWholeToken(str));
		str += strlen(token) + 1;
		if (isMixedNumber(token)) {
			strcpy(token, convertMixedNumberToFraction(token));
		}

		// if the token is a number, then:
		if (isTokenNumeric(token)) {
			// push it to the output queue.
			strcat(output, token);
			strcat(output, " ");
		} else if (isCharOperator(token[0])) {
			op = token[0];
			while (getTop(opStack) != NULL && shouldOpPop(*(char *)getTop(opStack), op)) {
				pop(opStack, &poppedOp);
				sprintf(tempOut, "%s%c ", output, poppedOp);
				strcpy(output, tempOut);
			}

			push(opStack, &op);
		} else if (token[0] == '(') {
			push(opStack, &token[0]);
		} else if (token[0] == ')') {
			while (*(char *)getTop(opStack) != '(' && height(opStack) != 0) {
				pop(opStack, &poppedOp);
				sprintf(tempOut, "%s%c ", output, poppedOp);
				strcpy(output, tempOut);
			}

			if (getTop(opStack) == NULL) {
				strcpy(output, "err: mismatched parentheses");
				return output;
			}

			pop(opStack, &poppedOp);
		} else {
			strcpy(output, "");
			return output;
		}
	}

	while (getTop(opStack) != NULL) {
		pop(opStack, &poppedOp);
		if (poppedOp == '(' || poppedOp == ')') {
			strcpy(output, "err: mismatched parentheses");
			return output;
		}

		sprintf(tempOut, "%s%c ", output, poppedOp);
		strcpy(output, tempOut);
	}

	return output;
}

char * getWholeToken(char * str) {
	int i = 0;
	char * ret = calloc(1, 22 * sizeof(char));
	if (isMixedNumber(str)) {
		int spaceCount = 0;

		// end token on 3 spaces
		while (spaceCount != 2) {
			if (str[i] == ' ')
				spaceCount++;
			i++;
		}

		strcpy(ret, str);
		ret[i - 1] = '\0';
		return ret;
	}

	while(str[i] != ' ' && isascii(str[i]) && str[i] != '\0') {
		ret[i] = str[i];
		i++;
	}

	ret[i] = '\0';
	return ret;
}

bool isTokenNumeric(char * str) {
	if (!isdigit(str[0]) && str[0] != '-')
		return false;

	if (str[0] == '-')
		return strlen(str) > 1 ? isdigit(str[1]) : false;

	return true;
}

bool isCharOperator(char c) {
	return strchr("*/+-^", c);
}

int getPrecedence(char op) {
	int table[128];
	table[94] = 4;
	table[42] = 3;
	table[47] = 3;
	table[43] = 2;
	table[45] = 2;

	return table[(int)op];
}

int getAssociativity(char op) {
	int table[128];
	table[94] = 1;
	table[42] = 0;
	table[47] = 0;
	table[43] = 0;
	table[45] = 0;

	return table[(int)op];
}

bool shouldOpPop(char topOfStack, char opToken) {
	return (
		(
			getPrecedence(topOfStack) > getPrecedence(opToken) ||
			(
				getPrecedence(topOfStack) == getPrecedence(opToken) &&
				getAssociativity(opToken)
			)
		) &&
		(
			topOfStack != '('
		)
	);
}

// there must be an easier method
bool isMixedNumber(char * str) {
	if (str[0] == '-')
		str += 1;

	if (!isdigit(str[0]))
		return false;

	int i = 1;
	while (isdigit(str[i]))
		i++;

	if (str[i] != ' ')
		return false;

	do
		i++;
	while (isdigit(str[i]));

	if (str[i] != '/')
		return false;

	return isdigit(str[i + 1]);
}

char * convertMixedNumberToFraction(char * mixed) {
	char * result = malloc(128 * sizeof(char));
	bool neg = 0;
	if (mixed[0] == '-') {
		neg = 1;
		mixed += 1;
	}

	char * num = strdup(mixed),
		 * den = strdup(mixed),
		 * coefficient = strdup(mixed);
	strtok(coefficient, " ");
	num += strlen(coefficient) + 1; // + 1 comes from space not
	den += strlen(coefficient) + 1; // contained in coefficient

	strtok(num, "/");
	den += strlen(num) + 1; // again, ignore space

	int totalNum = stoi(coefficient) * stoi(den) + stoi(num);
	sprintf(result, "%s%d/%d", neg ? "-" : "", totalNum, stoi(den)); // combine all into result string

	return result;
}

bool isValidPostfix(char * str, char * lastToken) {
	// ( 1 + 2 ) * 8 / 3 - 2 3/2 - -2
	char token[22];
	strcpy(token, getWholeToken(str));
	bool shouldContinueRecursing = strlen(token) != strlen(str); // if last token, don't recurse
	if (strlen(lastToken) == 0)
		// add one for space
		return shouldContinueRecursing ? isValidPostfix(str + strlen(token) + 1, token) : true;

	// if token is numeric, last token should be ( or operator
	if (isTokenNumeric(token)) {
		return shouldContinueRecursing
			// add one for space
			?   (
					(isCharOperator(lastToken[0]) && strlen(lastToken) == 1) ||
					lastToken[0] == '('
				) &&
				isValidPostfix(str + strlen(token) + 1, token)
			// can end on a number
			: true;

	// if token is a left parenthesis, then last token should be
	// operator or left parenthesis
	} else if (token[0] == '(') {
		return shouldContinueRecursing
			// add one for space
			? (
				lastToken[0] == '(' ||
				(strlen(lastToken) == 1 && isCharOperator(lastToken[0]))
			) && isValidPostfix(str + strlen(token) + 1, token)
			// no situation where ( is last token
			: false;

	// if last token is a right paren, last token
	// should've been numeric or a right paren
	} else if (token[0] == ')') {
		return shouldContinueRecursing
			? (
				isTokenNumeric(lastToken) ||
				lastToken[0] == ')'
			) && isValidPostfix(str + strlen(token) + 1, token)
			// can end on a )
			: true;

	// else token is operator
	// only right paren or numbers can precede
	} else if (isCharOperator(token[0])) {
		return shouldContinueRecursing
			? (
				lastToken[0] == ')' ||
				isTokenNumeric(lastToken)
			) && isValidPostfix(str + strlen(token) + 1, token)
			// cant end on an operator
			: false;
	}

	return false;
}