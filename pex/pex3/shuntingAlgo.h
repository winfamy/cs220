/** shuntingAlgo.h
* ===========================================================
* Name: Grady Phillips, 02 Mar, 2020
* Section: T6A
* Project: PEX3
* Purpose:
* 	Convert postfix algebraic notation to dumb
* 	RPN
* ===========================================================
*/

#ifndef CS220_SHUNTINGALGO_H
#define CS220_SHUNTINGALGO_H

#include <stdbool.h>
#include "../../lib/stack.h"

/**   ----------------------------------------------------------  
*   convert
*   Converts a valid postfix expression to RPN
*     
*   str: the string to convert to rpn  
*   
*   returns: char * rpn expression
*/ 
char * convert(char *);

/**   ----------------------------------------------------------
*   convertToDumbRPN
*   Handles the heavy work of the postfix -> RPN conversion
*
*   str: the input string
*   output: the string to output to
*   opStack: the operator stack used in the conversion
*
*   returns: char * the RPN expression
*/
char * convertToDumbRPN(char *, char * output, Stack * opStack);

/**   ----------------------------------------------------------
*   shouldOpPop
*   returns true/false based on that stupid long condition in
*   the wikipedia.
*
*	topOfStack: char op at the top of the opStack
*	opToken: token
*
*   returns: bool
*/
bool shouldOpPop(char topOfStack, char opToken);

/**   ----------------------------------------------------------
*   getWholeToken
*   returns a non-spaced substring from 0 -> n
*
*   str: the string to parse token from
*
*   returns: char * token
*/
char * getWholeToken(char *);

/**   ----------------------------------------------------------
*   isNumeric
*   returns if a string is a valid numeric expression
*   i.e. positive/negative and/or fractional
*
* 	str: input str
*
*   returns: bool
*/
bool isTokenNumeric(char * c);

/**   ----------------------------------------------------------  
*   isOperator
*   returns true if the input char is a valid operator
*     
*   c: input char
*   
*   returns:   
*/ 
bool isCharOperator(char c);


/**   ----------------------------------------------------------
*   isMixedNumber
*   returns true if detects r/\d+ \d+\/\d+/
*
*	token: token to check
*
*   returns: bool
*/
bool isMixedNumber(char * token);


/**   ----------------------------------------------------------
*   convertMixedNumberToFraction
*   converts a mixed number format 1 1/2 to fractional notation
*   1 1/2 -> 3/2
*
*   mixed: the mixed number input
*
*   returns: rational number as fraction str
*/
char * convertMixedNumberToFraction(char * mixed);

/**   ----------------------------------------------------------
*   getPrecedence
*   Gets the operator precedence based on a lookup table
*   written very hastily but seems to work well.
*
*   op: op char
*
*   returns: precedence 2 -> 5
*/
int getPrecedence(char op);

/**   ----------------------------------------------------------
*   getAssociativity
*   returns 1 if right associativity, 0 if left
*
*   op: operator to get associativity for
*
*   returns: 0/1 left/right
*/
int getAssociativity(char op);

/**   ----------------------------------------------------------  
*   isValidPostfix
*   returns true if given string is in valid postfix format
*     
*   str: string to check
*   
*   returns: bool
*/ 
bool isValidPostfix(char * str, char * lastToken);

/**   ----------------------------------------------------------
*   isTokenParenthesis
*   returns true if given string is a single ( or ) character
*
*   token: token to check
*
*   returns: bool
*/
bool isTokenParenthesis(char * token);
#endif //CS220_SHUNTINGALGO_H
