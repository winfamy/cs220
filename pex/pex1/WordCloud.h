/** WordCloud.h
 * ===========================================================
 * Name: CS220, Spring 2020
 *
 * Purpose:  Produces a word cloud given a word list and their
 * 			 frequency.
 * ===========================================================*/

#include "PEX1.h"
#ifndef WORDCLOUD_H

int BuildWordCloud(WordCount* argWordCount, STRING argFileName, int argNumWords); // MOVED

#define WORDCLOUD_H
#endif // !WORDCLOUD_H

