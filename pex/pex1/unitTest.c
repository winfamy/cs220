#include "PEX1.h"
#include "WordCloud.h"

int main(int argc, char** argv)
{
	// check that an argument was provided
	if (argc <= 1)
	{
		printf("Please provide a story file to read, an optional exclusion word list, and an ");
		printf("optional filename to create.\n\n");
		printf("USAGE:  PEX4.exe storyFile [exclusionWordFile] [outputFileName]\n");
		return 0;
	}

	printf("%s\n", argv[1]);
	printf("%s\n", argv[2]);
	printf("%s\n", argv[3]);
	// printf("%s\n", argv[4]);

	// create excluded words map and fill it in if the argument is provided
	WordCount* excludedWords = NULL;
	excludedWords = ReadExclusionFile(argv[2]);
	printf("after excluded\n");

	// create and fill in the story word map
	WordCount* storyWordMap = ReadStoryFile(argv[1], excludedWords);
	printf("after readstory\n");

	// dump the map into the parallel arrays
	int numUniqueWords = NumUniqueWords(storyWordMap);
	printf("after numunique\n");

	// if an output file is provided build the html word cloud
	if (argc > 3)
		BuildWordCloud(storyWordMap, argv[3], numUniqueWords);
	printf("build wordcloud\n");

	// sort the words by frequency, highest at the top
	QuickSortWords(storyWordMap, 0, numUniqueWords);
	printf("after sort\n");

	// print the words
	PrintWordsToConsole(storyWordMap);

	// release memory
	DeleteWordCount(excludedWords);
	DeleteWordCount(storyWordMap);

	printf("end\n");

	return 1;
}

